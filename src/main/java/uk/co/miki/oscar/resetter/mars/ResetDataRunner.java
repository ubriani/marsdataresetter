package uk.co.miki.oscar.resetter.mars;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uk.co.miki.oscar.resetter.csv.CustomResourceLoader;
import uk.co.miki.oscar.resetter.utils.ProductKey;

import java.util.List;


@Component
public final class ResetDataRunner  implements CommandLineRunner {

	Logger logger = LoggerFactory.getLogger(ResetDataRunner.class);
	CustomResourceLoader customResourceLoader;
	DataFetcherUpdater dataFetcherUpdater;
	public ResetDataRunner(CustomResourceLoader customResourceLoader, DataFetcherUpdater dataFetcherUpdater){
		this.customResourceLoader  = customResourceLoader;
		this.dataFetcherUpdater = dataFetcherUpdater;
	}




	@Override
	public void run(String... strings) throws Exception {

		customResourceLoader.getProducts().stream().parallel().forEach(productCode ->{
			try{
				processProduct(productCode);
			}catch(Exception e){
				logger.error("failed to process product "+ productCode);
			}
		});

//		for(String productCode : customResourceLoader.getProducts()){
//			try {
//				processProduct(productCode);
//			}catch(Exception e){
//				logger.error("failed to process product -->"+ productCode,e);
//			}
//		}

	}

	private void processProduct(String productCode) {
		logger.info("start product -->" + productCode);
		List<ProductKey> productKeys = dataFetcherUpdater.readKeysToUpdate(productCode);
		logger.info("read complete -->" + productCode + " total keys : " +productKeys.size());
		boolean isSuccess = dataFetcherUpdater.updateStatus(productCode, productKeys);
		logger.warn("end product -->" + productCode+ ",isSuccess :"+ isSuccess + ",total keys "+ productKeys.size() );
		System.out.println("end product -->" + productCode+ ",isSuccess :"+ isSuccess + ",total keys "+ productKeys.size() );
	}
}
