package uk.co.miki.oscar.resetter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@PropertySources({
		@PropertySource(value = "classpath:/uk/co/miki/ebiz/ari/application-${serverRole}.properties"),
})
@ImportResource({ "classpath:uk/co/miki/ebiz/ari/spring-config.xml" })
public class SpringBootConsoleApplication {

	private static Logger LOG = LoggerFactory
			.getLogger(SpringBootConsoleApplication.class);

	public static void main(String[] args) {
		LOG.info("STARTING THE APPLICATION");
		SpringApplication.run(SpringBootConsoleApplication.class, args);
		LOG.info("APPLICATION FINISHED");
	}


}