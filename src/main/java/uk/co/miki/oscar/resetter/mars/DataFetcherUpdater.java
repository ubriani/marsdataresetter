package uk.co.miki.oscar.resetter.mars;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import uk.co.miki.oscar.resetter.utils.Constants;
import uk.co.miki.oscar.resetter.utils.ProductKey;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
public final class DataFetcherUpdater {

    @Value("${number.of.servicedates}")
    private int numberOfServiceDates =375;

    @Value("${cassandra.mars.keyspace}")
    private String schema = "mars";

    private Map<String, PreparedStatement> selectByPKQueryMap_31001 = new ConcurrentHashMap<String, PreparedStatement>();
    private Map<String, PreparedStatement> selectByPKQueryMap_30001 = new ConcurrentHashMap<String, PreparedStatement>();
    private Map<String, PreparedStatement> updateByPKQueryMap = new ConcurrentHashMap<String, PreparedStatement>();

    CassandraConnection cassandraConnection;
    private static String TABLE = "mars_db_dd_mm_yyyy";
    private static String SELECT_BY_PK_31001 = "SELECT partitionkey,occupancytypeid,supplierid,subproductcode,priceid,sequenceid FROM schema.\"" + TABLE + "\" WHERE partitionkey = ? and status=1 and supplierid=43 and subproductcode=31001 allow filtering";
    private static String SELECT_BY_PK_30001 = "SELECT partitionkey,occupancytypeid,supplierid,subproductcode,priceid,sequenceid FROM schema.\"" + TABLE + "\" WHERE partitionkey = ? and status=1 and supplierid=43 and subproductcode=30001 allow filtering";
    private static String UPDATE_BY_PK = "UPDATE schema.\"" + TABLE + "\" set status=0 , lastupdatedtime=toTimeStamp(now()) " +
            "where partitionkey=? and occupancytypeid=? and supplierid=? and " +
            "subproductcode=? and priceid=? and sequenceid=?";

    private static final Logger LOGGER = LoggerFactory.getLogger(DataFetcherUpdater.class);

    public DataFetcherUpdater(CassandraConnection cassandraConnection) {
        this.cassandraConnection = cassandraConnection;
        buildInitialSelectStatementMap();
    }

    private void buildInitialSelectStatementMap() {
        Calendar serviceStartDate = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.JAVA_DATE_FORMAT);
        SELECT_BY_PK_31001 = SELECT_BY_PK_31001.replaceAll(Constants.SCHEMA, schema);
        SELECT_BY_PK_30001 = SELECT_BY_PK_30001.replaceAll(Constants.SCHEMA, schema);
        UPDATE_BY_PK = UPDATE_BY_PK.replaceAll(Constants.SCHEMA, schema);

        for (int i = 0; i <= numberOfServiceDates; i++) {

            String tableNameKey = simpleDateFormat.format(serviceStartDate.getTime());//e.g 01_02_2017
            if (selectByPKQueryMap_31001.get(tableNameKey) != null) {
                serviceStartDate.add(Calendar.DATE, 1);
                continue;
            }
            String selectSQL_31001 = SELECT_BY_PK_31001.replaceAll(Constants.TABLE_NAME_PATTERN, tableNameKey);
            String selectSQL_30001 = SELECT_BY_PK_30001.replaceAll(Constants.TABLE_NAME_PATTERN, tableNameKey);
            String updateSQL = UPDATE_BY_PK.replaceAll(Constants.TABLE_NAME_PATTERN, tableNameKey);

            PreparedStatement selectStatement_31001;
            PreparedStatement selectStatement_30001;
            PreparedStatement updateStatement;
            try {
                selectStatement_31001 = cassandraConnection.getGlobalInventorySession().prepare(selectSQL_31001);
                selectStatement_30001 = cassandraConnection.getGlobalInventorySession().prepare(selectSQL_30001);
                updateStatement = cassandraConnection.getGlobalInventorySession().prepare(updateSQL);
            } catch (Exception e) {
                LOGGER.error("Error creating prepared statement cache", e);
                throw new RuntimeException("Error creating prepared statement cache", e);
            }

            selectByPKQueryMap_31001.put(tableNameKey, selectStatement_31001);
            selectByPKQueryMap_30001.put(tableNameKey, selectStatement_30001);
            updateByPKQueryMap.put(tableNameKey, updateStatement);
            serviceStartDate.add(Calendar.DATE, 1);
        }
    }


    public List<ProductKey> readKeysToUpdate(String productCode) {
        List<ProductKey> productKeyList = new ArrayList<>();
        productKeyList.addAll(performReadForGivenSelection(productCode, this.selectByPKQueryMap_31001));
        System.out.println(" ================= Data read complete for 31001");
        productKeyList.addAll(performReadForGivenSelection(productCode, this.selectByPKQueryMap_30001));
        System.out.println(" ================= Data read complete for 30001");

        //LOGGER.debug("product code read complete "+ productCode + "total keys " + productKeyList.size());
        return productKeyList;
    }

    private List<ProductKey> performReadForGivenSelection(String productCode, Map<String, PreparedStatement> selectByPKQueryMap) {
        List<ProductKey> productKeyList = new ArrayList<>();
        int j =0;
        for (String tableName : selectByPKQueryMap.keySet()) {
            //LOGGER.debug("product code progress "+ productCode + "cnt "+ j++ + " total "+ selectByPKQueryMap_31001.keySet().size());
            PreparedStatement selectStmt = selectByPKQueryMap.get(tableName);
            Assert.notNull(selectStmt, "select stmt must exist");
            for (int i = 1; i <= 30; i++) {
                ResultSet selectRS = cassandraConnection.getGlobalInventorySession().execute(selectStmt.bind(productCode + "/" + i));
                List<Row> allRows = selectRS.all();
                for(Row row: allRows){
                    ProductKey productKey = new ProductKey();
                    productKey.setTableName(tableName);
                    productKey.setPartitionKey(row.getString("partitionkey"));
                    productKey.setOccupancyTypeId(row.getInt("occupancytypeid"));
                    productKey.setSupplierId(row.getInt("supplierid"));
                    productKey.setSubproductCode(row.getInt("subproductcode"));
                    productKey.setPriceId(row.getString("priceid"));
                    productKey.setSequenceId(row.getString("sequenceid"));
                    productKeyList.add(productKey);
                    System.out.println("data found "+ productKey);
                }
            }
        }
        return productKeyList;
    }

    public boolean updateStatus(String productCode, List<ProductKey> productKeys) {
        //LOGGER.debug("product update start "+ productCode + " - size  "+ productKeys.size());
        try {

            for (ProductKey productKey : productKeys) {
                Assert.isTrue(productKey.getSupplierId()==43, "supplierId must be 43 TS");
                PreparedStatement updateStmt = updateByPKQueryMap.get(productKey.getTableName());
                LOGGER.warn("update "+ updateStmt.getQueryString() + " parameters "+ productKey);
                Assert.notNull(updateStmt, "update stmt must exist");
                cassandraConnection.getGlobalInventorySession().execute(updateStmt.bind(productKey.getPartitionKey(), productKey.getOccupancyTypeId(),
                        productKey.getSupplierId(), productKey.getSubproductCode(), productKey.getPriceId(), productKey.getSequenceId()));
            }
        }catch(Exception e){
            LOGGER.error("Error updating table", e);
            return false;
        }
        //LOGGER.debug("product update end "+ productCode + " - size  "+ productKeys.size());
        return true;
    }
}
