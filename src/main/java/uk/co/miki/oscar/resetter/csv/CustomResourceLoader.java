package uk.co.miki.oscar.resetter.csv;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Component
public class CustomResourceLoader
{

	private static Logger LOG = LoggerFactory
			.getLogger(CustomResourceLoader.class);
	List<String> products = new ArrayList<>();

	@PostConstruct
	public void showResourceData() throws IOException
	{
		System.out.println("current directory ->"+ new File(".").getAbsolutePath());
		try (Scanner scanner = new Scanner(new File("./productsToReset.csv"));) {
			while (scanner.hasNextLine()) {
				String var = scanner.nextLine();
				LOG.debug("load product Code for reset -->"+ var);
				products.add(var);
			}
		}
	}

	public List<String> getProducts() {
		return products;
	}
}