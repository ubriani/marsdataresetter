package uk.co.miki.oscar.resetter.mars;

import com.datastax.driver.core.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;



public final class CassandraConnection {

	private Cluster cluster;
	private Session globalInventorySession;

	private String cassandraContactPoints;
	private int minConnections;
	private int maxConnections;
	private PoolingOptions options;

	@Value("${cassandra.mars.keyspace}")
	private String marsKeySpace;

	@Value("${cassandra.mikiAri.keyspace}")
	private String mikiAriKeySpace;

	public CassandraConnection(String cassandraContactPoints, int minConnections,
                               int maxConnections) {
		this.cassandraContactPoints = cassandraContactPoints;
		this.minConnections = minConnections;
		this.maxConnections = maxConnections;
		options = new PoolingOptions().setCoreConnectionsPerHost(HostDistance.LOCAL, minConnections)
				.setMaxConnectionsPerHost( HostDistance.LOCAL, maxConnections)
				.setCoreConnectionsPerHost(HostDistance.REMOTE, minConnections)
				.setMaxConnectionsPerHost( HostDistance.REMOTE, maxConnections);

		obtainCluster(options);
		obtainGlobalInventorySession();
	}

	private void obtainCluster(PoolingOptions options) {
		if (cluster == null || cluster.isClosed())
			cluster = Cluster.builder().addContactPoints(cassandraContactPoints.split(",")).
				withPoolingOptions(options).withSocketOptions(getSocketOptions()).build();
	}


	private void obtainGlobalInventorySession() {
		obtainCluster(options);
		globalInventorySession = cluster.connect(marsKeySpace);
	}

	private static SocketOptions getSocketOptions() {
		SocketOptions socketOptions = new SocketOptions()
				.setKeepAlive(true)
				.setConnectTimeoutMillis(5000)
				.setReadTimeoutMillis(12000);
		return socketOptions;
	}

	private PoolingOptions retrievePoolingOptions()  {
		PoolingOptions poolingOptions = new PoolingOptions();
		poolingOptions.setIdleTimeoutSeconds(120);
		poolingOptions.setHeartbeatIntervalSeconds(30);
		//poolingOptions.setPoolTimeoutMillis(5000);

		poolingOptions.setMaxRequestsPerConnection(HostDistance.LOCAL, 1024);
		poolingOptions.setMaxRequestsPerConnection(HostDistance.REMOTE, 256);

		poolingOptions.setMaxConnectionsPerHost(HostDistance.LOCAL, 1);
		poolingOptions.setMaxConnectionsPerHost(HostDistance.REMOTE, 1);

		poolingOptions.setCoreConnectionsPerHost(HostDistance.LOCAL, 1);
		poolingOptions.setCoreConnectionsPerHost(HostDistance.REMOTE, 1);

		poolingOptions.setNewConnectionThreshold(HostDistance.LOCAL, 800);
		poolingOptions.setNewConnectionThreshold(HostDistance.REMOTE, 200);

		return poolingOptions;
	}

	public Session getGlobalInventorySession() {
		if(globalInventorySession == null || globalInventorySession.isClosed())
			obtainGlobalInventorySession();
		return globalInventorySession;
	}

	public Cluster getCluster() {
		return cluster;
	}
}
