package uk.co.miki.oscar.resetter.utils;


import lombok.Data;

@Data
public class ProductKey {
	String tableName;
	String partitionKey;
	int occupancyTypeId;
	int supplierId;
	int subproductCode;
	String priceId;
	String sequenceId;

}
